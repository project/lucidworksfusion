<?php

namespace Drupal\fusion_connector\ResourceType;

use Drupal\Core\Session\AccountProxyInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\Core\Entity\EntityTypeBundleInfoInterface;
use Drupal\jsonapi\ResourceType\ResourceTypeRepositoryInterface;

/**
 * Provides a repository of JSON:API configurable resource types.
 */
class FusionConnectorResourceTypeRepository {

  const BUNDLETYPES = [
    'node', 'taxonomy_term', 'taxonomy_vocabulary', 'media', 'media_type', 'file',
  ];

  /**
   * The JSON:API resource type repository.
   *
   * @var \Drupal\jsonapi\ResourceType\ResourceTypeRepositoryInterface
   */
  protected $resourceTypeRepository;

  /**
   * The bundle manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeBundleInfoInterface
   */
  protected $entityTypeBundleInfo;

  /**
   * Language manager.
   *
   * @var \Drupal\Core\Language\LanguageManagerInterface
   */
  protected $languageManager;

  /**
   * The aggregator.settings config object.
   *
   * @var \Drupal\Core\Config\Config
   */
  protected $config;

  /**
   * Current user.
   *
   * @var \Drupal\Core\Session\AccountProxyInterface
   */
  protected $currentUser;

  /**
   * ResourceTypeConverter constructor.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The factory for configuration objects.
   * @param \Drupal\jsonapi\ResourceType\ResourceTypeRepositoryInterface $resource_type_repository
   *   The JSON:API resource type repository.
   * @param \Drupal\Core\Entity\EntityTypeBundleInfoInterface $entity_bundle_info
   *   The entity type bundle info service.
   * @param \Drupal\Core\Language\LanguageManagerInterface $language_manager
   *   Language manager.
   * @param \Drupal\Core\Session\AccountProxyInterface $current_user
   *   Current user.
   */
  public function __construct(
    ConfigFactoryInterface $config_factory,
    ResourceTypeRepositoryInterface $resource_type_repository,
    EntityTypeBundleInfoInterface $entity_bundle_info,
    LanguageManagerInterface $language_manager,
    AccountProxyInterface $current_user
  ) {
    $this->resourceTypeRepository = $resource_type_repository;
    $this->entityTypeBundleInfo = $entity_bundle_info;
    $this->languageManager = $language_manager;
    $this->config = $config_factory->get('fusion_connector.settings');
    $this->currentUser = $current_user;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('current_user'),
      $container->get('config.factory'),
      $container->get('language_manager')
    );
  }

  /**
   * Get all the available resource types, after the filtering is applied.
   *
   * {@inheritdoc}
   */
  public function getAllAvailableResourceTypes() {

    $user = $this->currentUser;
    $disabledLanguages = $this->config->get('disabled_languages') ? $this->config->get(
      'disabled_languages'
    ) : [];
    $disabled_entities = $this->config->get('disabled_entities') ? $this->config->get(
      'disabled_entities'
    ) : [];
    $currentLanguage = $this->languageManager->getCurrentLanguage()->getId(
    );

    $resources = [];
    if (!in_array($currentLanguage, $disabledLanguages)) {
      $allResources = $this->resourceTypeRepository->all();
      foreach ($allResources as $key => $resource) {
        $resource_config_id = sprintf(
          '%s--%s',
          $resource->getEntityTypeId(),
          $resource->getBundle()
        );
        $disabledLanguages = $this->config->get(
            'disabled_entity_type_languages'
          )[$resource_config_id] ?? [];
        if (!in_array(
            $currentLanguage,
            $disabledLanguages
          ) && !$resource->isInternal() && in_array(
            $resource->getEntityTypeId(),
            self::BUNDLETYPES
          ) && $user->hasPermission(
            'view fusion_connector ' . $key
          ) && !in_array($key, $disabled_entities)) {
          $resources[$key] = $resource;
        }
      }
    }
    else {
      $resources = [];
    }

    return $resources;
  }

  /**
   * Get all the enabled resource types.
   *
   * {@inheritdoc}
   */
  public function getAllEnabledResourceTypes() {
    $disabled_entities = $this->config->get('disabled_entities') ? $this->config->get(
      'disabled_entities'
    ) : [];

    $resources = [];

    $allResources = $this->resourceTypeRepository->all();
    foreach ($allResources as $key => $resource) {
      if (!$resource->isInternal() && in_array(
          $resource->getEntityTypeId(),
          self::BUNDLETYPES
        ) && !in_array($key, $disabled_entities)) {
        $resources[$key] = $resource;
      }
    }

    return $resources;
  }

  /**
   * Get all the available resource types, no filtering is applied.
   *
   * {@inheritdoc}
   */
  public function getAllAvailableResourceTypesNoFilters() {
    $disabled_entities = $this->config->get('disabled_entities') ? $this->config->get(
      'disabled_entities'
    ) : [];
    $resources = [];

    foreach (self::BUNDLETYPES as $value) {
      $bundleInfo = $this->entityTypeBundleInfo->getBundleInfo(
        $value
      );
      if (count($bundleInfo)) {
        foreach ($bundleInfo as $bundle => $entitiesArray) {
          foreach ($entitiesArray as $key => $label) {
            $resource_config_id = sprintf(
              '%s--%s',
              $value,
              $bundle
            );
            // Hide config for disabled entities.
            if (!in_array($resource_config_id, $disabled_entities)) {
              $resources[$value][$bundle] = $entitiesArray;
            }
          }
        }
      }
    }

    return $resources;

  }

}
